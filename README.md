# Simulator

## ToC
| Spring Cloud components | Optional | Resources  |
|-------------------------|------------|------------|
| Docker Compose          | | [docker-compose.yml](docker-compose.yml) |
| Configuration server    | | http://localhost:8888 with [Configuration repository](https://bitbucket.org/minway/hackathon201903spring-config/src/master/microservices-config) |
| Eureka Discovery Service| | http://localhost:8761 and [Service discovery client] Services availability track by Eureka dashboard |
| Hystrix Dashboard for Circuit Breaker         | | http://localhost:7979 [Circuit Breaker with Hystrix guide](https://spring.io/guides/gs/circuit-breaker/) |
| Hystrix Event stream    | | `http://localhost:8080/actuator/hystrix.stream` / `http://api-gateway:8080/actuator/hystrix.stream` |
| Feature services        | | [Adserver] [Bidder] [Campaign Admin] [DCL] [Target Manager] - random port, check Eureka Dashboard|
| Tracing Server (Zipkin) | yes | http://localhost:9411/zipkin/ by [Open Zipkin](https://github.com/openzipkin/zipkin/tree/master/zipkin-server) |
| Spring Boot Admin       | yes | http://localhost:9090 |
| API Gateway             | | [Zuul reverse proxy] and [Routing configuration] |

## Starting services locally 
### Standalone
* Spring Boot application can be started locally using IDE or `mvn spring-boot:run`

* Technical support services (Config and Discovery Server) *MUST* be started before any feature application

* Config Server can use local Git repository by using `local` profile and setting
`GIT_REPO` environment variable:
`-Dspring.profiles.active=local -DGIT_REPO=/projects/spring-microservices-config`

### With docker-compose
* Build images from a (sub)project root by `mvn clean install -PbuildDocker` 
* Once images are ready, do `docker-compose up`

Containers startup order is coordinated with [dockerize](https://github.com/jwilder/dockerize). 

After starting services it takes a while for API Gateway to be in sync with service registry,
so don't be scared of initial Zuul timeouts. 

Main api : `http://{bidder_url}/api/bid`
