package application.model;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

@Data
@Builder
@NoArgsConstructor(force = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Target {

    @NotEmpty
    @Pattern(regexp = "^[A-Za-z0-9-_]*$", message = "must contain only letters, numbers, - and _")
    private String uuid;

    @NotEmpty
    @Pattern(regexp = "^[A-Za-z0-9-_]*$", message = "must contain only letters, numbers, - and _")
    private String name;

}
