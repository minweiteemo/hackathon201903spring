package application.model;

import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class TargetRepository {
    public List<Target> findAll() {
        Target target1 = Target.builder().name("DU1").uuid("ajsfsdifojds-sdfasdf-eawgaweg").build();
        Target target2 = Target.builder().name("DU2").uuid("de-vi-ce-uuid").build();
        try {
            Thread.sleep((long)(Math.random() * 10));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return Arrays.asList(target1, target2);
    }
}
