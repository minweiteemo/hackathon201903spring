package application.web;

import application.model.Target;
import application.model.TargetRepository;
import lombok.RequiredArgsConstructor;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/api")
@RestController
@RequiredArgsConstructor
class TargetController {

    private final TargetRepository targetRepository;

    @GetMapping("/targets")
    public List<Target> getTargets() {
        return targetRepository.findAll();
    }
}
