package application.system;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "targets")
public class CacheProperties {
    private Cache cache;

    @Data
    public static class Cache {
        private int ttl;
        private int heapSize;
    }
}
