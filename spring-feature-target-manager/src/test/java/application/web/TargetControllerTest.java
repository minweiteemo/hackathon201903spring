package application.web;

import application.model.Target;
import application.model.TargetRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(TargetController.class)
@ActiveProfiles("test")
class TargetControllerTest {

    @Autowired
    MockMvc mvc;

    @MockBean
    private TargetRepository targetRepository;

    @Test
    void shouldGetCampaigns() throws Exception {
        Target target = new Target();
        String name = "YourName";
        target.setName(name);

        given(targetRepository.findAll()).willReturn(Collections.singletonList(target));

        mvc.perform(get("/api/targets").accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[0].name").value(name));
    }
}
