
package application.web;

import application.model.Campaign;
import application.model.CampaignRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(CampaignController.class)
@ActiveProfiles("test")
class CampaignControllerTest {

    @Autowired
    MockMvc mvc;

    @MockBean
    private CampaignRepository campaignRepository;

    @Test
    void shouldGetCampaigns() throws Exception {
        Campaign campaign = new Campaign();
        String name = "YourName";
        campaign.setName(name);

        given(campaignRepository.findAll()).willReturn(Collections.singletonList(campaign));

        mvc.perform(get("/api/campaigns").accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[0].name").value(name));
    }
}
