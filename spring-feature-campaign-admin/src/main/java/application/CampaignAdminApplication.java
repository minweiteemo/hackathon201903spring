
package application;

import application.system.CacheProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
@EnableConfigurationProperties(CacheProperties.class)
public class CampaignAdminApplication {

	public static void main(String[] args) {
		SpringApplication.run(CampaignAdminApplication.class, args);
	}
}
