package application.model;

import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class CampaignRepository {
    public List<Campaign> findAll() {
        Campaign campaign1 = Campaign.builder().name("MyName").advertiserDomain("goo.gle").campaignManager("abc@teemo.co").build();
        Campaign campaign2 = Campaign.builder().name("YourName").advertiserDomain("ya.hoo").campaignManager("abc@teemo.co").build();
        return Arrays.asList(campaign1, campaign2);
    }
}
