package application.model;

import lombok.*;
import lombok.Builder.Default;
import lombok.experimental.Wither;

import javax.validation.constraints.*;

@Data
@Builder
@NoArgsConstructor(force = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Campaign {
    @Wither
    @Positive
    private final Integer id;

    @NotEmpty
    @Pattern(regexp = "^[A-Za-z0-9-_]*$", message = "must contain only letters, numbers, - and _")
    private String name;

    @NotEmpty
    @Pattern(regexp = "^([a-z0-9]+(-[a-z0-9]+)*\\.)+[a-z]{2,}$", message = "must contain only one level (e.g teemo.co, joann.com, ...)")
    private String advertiserDomain;

    @NotNull
    private Type type;

    @NotNull
    private Integer postViewDuration;

    @NotNull
    private Currency currency;

    @PositiveOrZero
    @NotNull
    private Long budget;

    @NotNull
    @Default
    private Status status = Status.DRAFT;

    @NotNull
    @Positive
    private Integer clientId;

    @NotNull
    @Positive
    private Integer impressionLimit;

    @NotNull
    @Positive(message = "must be in ]0, 15]")
    @Max(value = 15, message = "must be in ]0, 15]")
    private Integer impressionLimitTimeSpan;

    @Positive
    private Integer agencyId;

    @NotNull
    @Email(regexp = ".*@teemo\\.co$", message = "must be a well-formed teemo email address")
    private String sales;

    @NotNull
    @Email(regexp = ".*@teemo\\.co$", message = "must be a well-formed teemo email address")
    private String accountStrategist;

    @NotNull
    @Email(regexp = ".*@teemo\\.co$", message = "must be a well-formed teemo email address")
    private String campaignManager;

    public enum Type {
        CPM, CPV
    }

    public enum Currency {
        EUR, USD
    }

    public enum Status {
        DRAFT, LIVE, READY, PAUSED, ENDED
    }
}
