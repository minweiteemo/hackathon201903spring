
package application.web;

import application.model.Capping;
import application.model.CappingRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(CappingController.class)
@ActiveProfiles("test")
class CappingControllerTest {

    @Autowired
    MockMvc mvc;

    @MockBean
    private CappingRepository cappingRepository;

    @Test
    void shouldGetCampaigns() throws Exception {
        Capping capping = new Capping();
        String name = "YourName";
        capping.setCampaign(name);

        given(cappingRepository.findAll()).willReturn(Collections.singletonList(capping));

        mvc.perform(get("/api/cappings").accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[0].campaign").value(name));
    }
}
