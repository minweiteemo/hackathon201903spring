package application.model;

import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class CappingRepository {
    public List<Capping> findAll() {
        Capping capping1 = Capping.builder().campaign("Camp1").device("sdfasf-eawgweg-wefawef").build();
        Capping capping2 = Capping.builder().campaign("Camp2").device("sdsfewa-aege-egaw").build();
        return Arrays.asList(capping1, capping2);
    }
}
