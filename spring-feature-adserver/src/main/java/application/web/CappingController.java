package application.web;

import application.model.Capping;
import application.model.CappingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("/api")
@RestController
@RequiredArgsConstructor
class CappingController {

    private final CappingRepository cappingRepository;

    @GetMapping("/cappings")
    public List<Capping> getCappings() {
        return cappingRepository.findAll();
    }
}
