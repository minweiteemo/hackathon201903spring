package application;

import application.system.CacheProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
@EnableConfigurationProperties(CacheProperties.class)
public class AdserverApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdserverApplication.class, args);
	}
}
