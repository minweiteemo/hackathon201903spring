package application.repository;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Bean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Component
public class CommunicationRepository {
    @Autowired
    private DiscoveryClient discoveryClient;

    @Autowired
    private RestTemplate campaignsRestTemplate;

    @Autowired
    private AdserverClient adserverClient;

    @Autowired
    private TargetManagerClient targetManagerClient;

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    public void discoverDependencies() {
        discoveryClient.getInstances("adserver").forEach((ServiceInstance s) -> {
            System.out.println("Getting from Adserver - " + s.getUri() + " | " + ToStringBuilder.reflectionToString(s));
            System.out.println(adserverClient.getCappings());
        });
        discoveryClient.getInstances("campaign-admin").forEach((ServiceInstance s) -> {
            System.out.println("Getting from Campaign Admin - " + s.getUri() + " | " + ToStringBuilder.reflectionToString(s));
            System.out.println(getCampaigns(s.getUri().toString()));
        });
        discoveryClient.getInstances("target-manager").forEach((ServiceInstance s) -> {
            System.out.println("Getting from Target Manager - " + s.getUri() + " | " + ToStringBuilder.reflectionToString(s));
            System.out.println(targetManagerClient.getTargets());
        });
    }

    /**
     * RestTemplate
     */
    private JSONObject getCampaigns(String apiUrl) {
        ResponseEntity<JSONObject> exchange =
                this.campaignsRestTemplate.exchange(
                        "http://" + apiUrl + "/campaigns",
                        HttpMethod.GET,
                        null,
                        new ParameterizedTypeReference<JSONObject>() {
                        });
        return exchange.getBody();
    }

    /**
     * Feign
     */
    @FeignClient(name = "adserver")
    interface AdserverClient {
        @RequestMapping(method = RequestMethod.GET, value = "/cappings")
        List<JSONObject> getCappings();
    }

    @FeignClient(name = "target-manager")
    interface TargetManagerClient {
        @RequestMapping(method = RequestMethod.GET, value = "/targets")
        List<JSONObject> getTargets();
    }
}
