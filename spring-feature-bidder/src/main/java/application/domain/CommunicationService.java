package application.domain;

import application.repository.CommunicationRepository;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class CommunicationService {

    @Autowired
    private CommunicationRepository communicationRepository;

    public String getAllDspThings() {
        communicationRepository.discoverDependencies();
        Map<String, String> resultMap = new HashMap<String, String>() {{
            put("status", "OK");
        }};
        return new JSONObject(resultMap).toString();
    }

}