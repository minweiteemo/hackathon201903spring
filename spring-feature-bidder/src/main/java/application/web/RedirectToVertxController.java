package application.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

@Controller
public class RedirectToVertxController {
    private String server = "localhost";
    private int vertxRedirectPort = 16688;

    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping("/api/**")
    @ResponseBody
    public String mirrorRest(HttpMethod method, HttpServletRequest request) throws URISyntaxException {
        URI uri = new URI("http", null, server, vertxRedirectPort, request.getRequestURI(), request.getQueryString(), null);

        ResponseEntity<String> responseEntity =
                restTemplate.exchange(uri, method, null, String.class);

        return responseEntity.getBody();
    }
}
