
package application;

import application.web.CommunicationVerticle;
import application.web.ServerVerticle;
import io.vertx.core.Vertx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

import javax.annotation.PostConstruct;

@EnableFeignClients(basePackages = {"application.repository", "application.web"})
@EnableEurekaClient
@EnableDiscoveryClient
@SpringBootApplication
public class DCLApplication {

    @Autowired
    private ServerVerticle serverVerticle;

    @Autowired
    private CommunicationVerticle communicationVerticle;

    public static void main(String[] args) {
        SpringApplication.run(DCLApplication.class, args);
    }

    @PostConstruct
    public void deployVerticle() {
        Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(serverVerticle);
        vertx.deployVerticle(communicationVerticle);
    }
}
