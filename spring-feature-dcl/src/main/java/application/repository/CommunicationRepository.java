package application.repository;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Component
public class CommunicationRepository {
    @Autowired
    private DiscoveryClient discoveryClient;

    @Autowired
    private AdserverClient adserverClient;

    public void discoverDependencies() {
        discoveryClient.getInstances("adserver").forEach((ServiceInstance s) -> {
            System.out.println("Getting from Adserver - " + s.getUri() + " | " + ToStringBuilder.reflectionToString(s));
            System.out.println(adserverClient.getCappings());
        });

    }

    /**
     * Feign
     */
    @FeignClient(name = "adserver")
    interface AdserverClient {
        @RequestMapping(method = RequestMethod.GET, value = "/cappings")
        List<JSONObject> getCappings();
    }
}
